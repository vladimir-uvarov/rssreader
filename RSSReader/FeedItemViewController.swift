//
//  FeedItemViewController.swift
//  RSSReader
//
//  Created by vladimir on 12.04.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import Foundation
import WebKit

class FeedItemViewController: UIViewController, WKUIDelegate {
    
    var webView: WKWebView!
    var urlString: String?
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let myURL = URL(string: urlString!)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
}
