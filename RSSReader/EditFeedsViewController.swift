//
//  EditFeedsViewController.swift
//  RSSReader
//
//  Created by vladimir on 14.04.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit
import RealmSwift

class EditFeedsViewController: UITableViewController {
    var feeds  = try! Realm().objects(Feed.self)
    var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        tableView.tableFooterView = UIView()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(EditFeedsViewController.doneAction))
        setupLabel()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func setupLabel(){
        titleLabel = UILabel()
        titleLabel.textColor = UIColor.white
        titleLabel.textAlignment = .center
        titleLabel.text = "Tap a feed to delete it"
        titleLabel.backgroundColor = UIColor.black
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return titleLabel
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = feeds[indexPath.row].title
        return cell
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deleteFeed(indexPath)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if feeds.isEmpty {
            TableViewEmpty.setPlaceholder("No feeds present.", viewController: self)
            return 0
        } else {
            return 1
        }
    }
    
    func deleteFeed(_ indexPath : IndexPath) {
        let itemToRemove = feeds[indexPath.row]
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.delete(itemToRemove)
            }
        }  catch let error as NSError {
            fatalError(error.localizedDescription)
        }
        
        tableView.reloadData()
    }
    
    func doneAction() {
        _ = navigationController?.popViewController(animated: true)
    }
}
