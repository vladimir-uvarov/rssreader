//
//  FeedCell.swift
//  RSSReader
//
//  Created by vladimir on 12.04.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {
    
    var titleLabel: UILabel!
    var descriptionLabel: UILabel!
    var previewImage: UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
        setLabels()
        setImage()
        adjustConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setLabels(){
        descriptionLabel = UILabel()
        titleLabel =  UILabel()
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.font = UIFont(name: titleLabel.font.fontName, size: 11)
        descriptionLabel.font = UIFont(name: titleLabel.font.fontName, size: 11)
        descriptionLabel.numberOfLines = 3
        
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(descriptionLabel)
    }
    
    func setImage() {
        previewImage = UIImageView()
        previewImage.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(previewImage)
        
    }
    
    func adjustConstraints() {
        var allConstraints = [NSLayoutConstraint]()
        let views = ["cell": self.contentView, "titleLabel" : titleLabel, "descriptionLabel" : descriptionLabel, "image" : previewImage] as [String : Any]
        let titleLabelHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[image(50)]-10-[titleLabel]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += titleLabelHorizontalConstraints
        let titleLabelVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:[titleLabel]-[descriptionLabel]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += titleLabelVerticalConstraints
        let descriptionLabelHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[image(50)]-10-[descriptionLabel]-10-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += descriptionLabelHorizontalConstraints
        
        let imageVerticalConstraint = NSLayoutConstraint(item: previewImage, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0)
        let imageHeightConstraint = NSLayoutConstraint(item: previewImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute:.notAnAttribute, multiplier: 1, constant: 50)
        NSLayoutConstraint.activate([imageHeightConstraint, imageVerticalConstraint])
        
        
        NSLayoutConstraint.activate(allConstraints)
    }
    
    
}
