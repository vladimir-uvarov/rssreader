//
//  ViewController.swift
//  RSSReader
//
//  Created by vladimir on 11.04.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit
import RealmSwift
import MWFeedParser
import AlamofireImage


class FeedListViewController: UITableViewController {
    var feeds  = try! Realm().objects(Feed.self)
    let feedParser = MWFeedParser()
    var parsedItems = [MWFeedItem]()
    var storedItems = [Item]()
    var feedsParsedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action:  #selector (loadFeeds), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl!)
        tableView.register(FeedCell.self, forCellReuseIdentifier: "taskCell")
        
        //        loadFeeds()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.title = "RSS Feeds"
        let addFeedButton = UIBarButtonItem(title: "Add Source", style: .done, target: self,
                                            action: #selector (addNewFeed))
        let editFeedsButton = UIBarButtonItem(title: "Edit Feeds", style: .done, target: self,
                                              action: #selector (editFeeds))
        navigationItem.rightBarButtonItem = addFeedButton
        navigationItem.leftBarButtonItem = editFeedsButton
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        loadFeeds()
    }
    
    func addNewFeed() {
        let vc = AddFeedViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func editFeeds() {
        let vc = EditFeedsViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadFeeds() {
        if feeds.isEmpty {
            self.refreshControl?.endRefreshing()
        } else {
            for feed in self.feeds {
                requestFeed(feed.link!)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return feeds[section].title
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if feeds.isEmpty {
            TableViewEmpty.setPlaceholder("No feeds present.", viewController: self)
            return 0
        } else {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
            return feeds.count
        }
    }
    
    override func tableView(_ tableView: (UITableView!), numberOfRowsInSection section: Int) -> Int {
        return feeds[section].items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! FeedCell
        cell.descriptionLabel?.text = feeds[indexPath.section].items[indexPath.row].itemDescription
        cell.titleLabel?.text = feeds[indexPath.section].items[indexPath.row].title
        cell.previewImage.image = UIImage(named: "placeholder")
        
        cell.previewImage?.contentMode = UIViewContentMode.scaleAspectFit
        var imgURL: URL?
        
        if let imgUrlString = feeds[indexPath.section].items[indexPath.row].imageLink {
            imgURL = URL(string: imgUrlString)
            cell.previewImage.af_setImage(withURL: imgURL!)
        }
        return cell
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewWebPage(indexPath)
    }
    
    func viewWebPage(_ indexPath:IndexPath){
        let vc = FeedItemViewController()
        vc.urlString = feeds[indexPath.section].items[indexPath.row].link
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension FeedListViewController: MWFeedParserDelegate {
    
    func feedParser(_ parser: MWFeedParser!, didParseFeedItem item: MWFeedItem!) {
        
        let storedItem = Item()
        storedItem.source = "\(parser.url!)"
        storedItem.title = item.title
        storedItem.link = item.link
        storedItem.itemDescription = item.summary
        if item.enclosures != nil {
            storedItem.imageLink = parseEnclosures(item.enclosures)
        }
        
        storedItems.append(storedItem)
        
    }
    func feedParserDidFinish(_ parser: MWFeedParser!) {
        feedsParsedIndex += 1
        if feedsParsedIndex == feeds.count {
            for feed in self.feeds {
                mapObjects(feed)
            }
            feedsParsedIndex = 0
            storedItems.removeAll()
            self.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    func feedParser(_ parser: MWFeedParser!, didFailWithError error: Error!) {
        self.refreshControl?.endRefreshing()
    }
    
    func requestFeed(_ rssString: String) {
        
        let url = URL(string: rssString)
        let feedParser = MWFeedParser(feedURL: url)
        feedParser?.delegate = self
        feedParser?.connectionType = ConnectionTypeAsynchronously
        feedParser?.feedParseType = ParseTypeFull
        feedParser?.parse()
    }
    
    func mapObjects(_ feed: Feed){
        do {
            let realm = try Realm()
            
            try realm.write {
                feed.items.removeAll()
                for item in storedItems {
                    if feed.link == item.source{
                        feed.items.append(item)
                    }
                }
            }
            
            
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
    
    func parseEnclosures(_ enclosures: [Any]!) -> String? {
        for enclosure in enclosures {
            guard let dict = enclosure as? [String: Any] else {
                return nil
            }
            guard let type =  dict["type"] as? String  else {
                return nil
            }
            if type == "image/jpeg" {
                guard let  imageURL =  dict["url"] as? String? else {
                    return nil
                }
                return imageURL
                
            }
        }
        return nil
    }
    
}

class TableViewEmpty {
    
    class func setPlaceholder(_ message:String, viewController:UITableViewController) {
        let messageLabel = UILabel(frame: CGRect(x:0,y:0,width:viewController.view.bounds.size.width, height:viewController.view.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.gray
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.sizeToFit()
        viewController.tableView.backgroundView = messageLabel;
        viewController.tableView.separatorStyle = .none;
    }
}

import ObjectiveC

private var associationKey: UInt8 = 0

extension MWFeedItem {
    var source: String! {
        get {
            return objc_getAssociatedObject(self, &associationKey) as? String
        }
        set(newValue) {
            objc_setAssociatedObject(self, &associationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}



