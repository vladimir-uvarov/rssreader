//
//  Item.swift
//  RSSReader
//
//  Created by vladimir on 12.04.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    dynamic var title: String?
    dynamic var link: String?
    dynamic var itemDescription: String?
    dynamic var source: String?
    dynamic var imageLink: String?
}
