//
//  AddFeedViewController.swift
//  RSSReader
//
//  Created by vladimir on 12.04.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit
import RealmSwift

class AddFeedViewController: UIViewController, UITextFieldDelegate {
    
    var nameTextField: UITextField? = UITextField()
    var linkTextField: UITextField? = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = "New Feed"
        setupTextField(nameTextField, "Feed name")
        setupTextField(linkTextField, "Feed URL")
        adjustConstraints()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(AddFeedViewController.doneAction))
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTextField(_ textField: UITextField?, _ placeholder: String?) {
        textField?.frame = CGRect.zero
        textField?.translatesAutoresizingMaskIntoConstraints = false
        textField?.delegate = self
        textField?.placeholder = placeholder
        view.addSubview(textField!)
        
    }
    
    func doneAction() {
        
        if ((self.nameTextField?.text?.utf16.count)! > 0 &&  (self.linkTextField?.text?.utf16.count)! > 0) {
            let newFeed = Feed()
            newFeed.link = linkTextField?.text
            newFeed.title = nameTextField?.text
            
            if verifyUrl(urlString: linkTextField?.text) {
                
                do {
                    let realm = try Realm()
                    try realm.write {
                        realm.add(newFeed)
                    }
                } catch let error as NSError {
                    fatalError(error.localizedDescription)
                }
                
                _ = navigationController?.popViewController(animated: true)
                
            }
        }
    }
    
    func adjustConstraints(){
        let nameTextFieldTopConstraint = NSLayoutConstraint(item: nameTextField!, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 100)
        let nameTextFieldCenterConstraint = NSLayoutConstraint(item: nameTextField!, attribute: .centerX, relatedBy: .equal, toItem: view, attribute:.centerX, multiplier: 1.0, constant: 0)
        let nameTextFieldLeadingConstraint = NSLayoutConstraint(item: nameTextField!, attribute: .leading, relatedBy: .equal, toItem: view, attribute:.leading, multiplier: 1.0, constant: 18)
        let nameTextFieldHeightConstraint = NSLayoutConstraint(item: nameTextField!, attribute: .height, relatedBy: .equal, toItem: nil, attribute:.notAnAttribute, multiplier: 1, constant:120)
        let linkTextFieldFieldTopConstraint = NSLayoutConstraint(item: linkTextField!, attribute: .top, relatedBy: .equal, toItem: nameTextField!, attribute: .bottom, multiplier: 1, constant: 0)
        let linkTextFieldFieldCenterConstraint = NSLayoutConstraint(item: linkTextField!, attribute: .centerX, relatedBy: .equal, toItem: view, attribute:.centerX, multiplier: 1.0, constant:0)
        let linkTextFieldFieldLeadingConstraint = NSLayoutConstraint(item: linkTextField!, attribute: .leading, relatedBy: .equal, toItem: view, attribute:.leading, multiplier: 1.0, constant:18)
        let linkTextFieldHeightConstraint = NSLayoutConstraint(item: linkTextField!, attribute: .height, relatedBy: .equal, toItem: nil, attribute:.notAnAttribute, multiplier: 1, constant:20)
        NSLayoutConstraint.activate([nameTextFieldTopConstraint,
                                     nameTextFieldCenterConstraint,
                                     nameTextFieldHeightConstraint,
                                     nameTextFieldLeadingConstraint,
                                     linkTextFieldFieldTopConstraint,
                                     linkTextFieldFieldCenterConstraint,
                                     linkTextFieldFieldLeadingConstraint,
                                     linkTextFieldHeightConstraint])
        
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url = URL(string: urlString) {
                return UIApplication.shared.canOpenURL(url)
            }
        }
        return false
    }
}
