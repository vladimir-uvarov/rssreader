//
//  Feed.swift
//  RSSReader
//
//  Created by vladimir on 12.04.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import Foundation
import RealmSwift

open class Feed: Object {
    dynamic var title: String? = ""
    dynamic var link: String? = ""
    let items =  List<Item>()
}
